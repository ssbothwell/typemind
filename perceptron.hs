{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Perceptron where

import Control.Monad
import Control.Monad.Zip

import Data.Monoid
import Data.Monoid.Action

import Test.QuickCheck

newtype Vector a = Vector { _vector :: [a] }
  deriving (Eq, Show, Foldable, Functor, Applicative, Monad, MonadZip)

instance Num a => Monoid (Vector a) where
  mempty = Vector $ repeat 0
  mappend = mzipWith (+)

instance Num a => Action a (Vector a) where
  act :: a -> Vector a -> Vector a
  act s = fmap (* s)

instance (Foldable t) => Action (t (a -> a)) a where
  act :: t (a -> a) -> a -> a
  act = foldr (.) id
  
newtype Weight = Weight { _weight :: Vector Double } deriving Show
newtype Point  = Point  { _point  :: Vector Double } deriving Show

type Label = Double
type Datum = (Point,Label)
type Data  = [Datum]


--------------------
---- Operations ----
--------------------

scalarMultiply :: Double -> Vector Double -> Vector Double
scalarMultiply = act

dot :: Num a => Vector a -> Vector a -> a
dot xs ys = sum $ mzipWith (*) xs ys


scalarMultCompose :: [Double] -> (Vector Double -> Vector Double)
scalarMultCompose = foldr (.) id . fmap act

--------------------
---- Perceptron ----
--------------------

implement :: Weight -> Point -> Label
implement (Weight wt) (Point pt) = signum $ wt `dot` pt

update :: Datum -> Weight -> Weight
update (pt, lbl) wt =
  if implement wt pt == lbl
  then wt
  else Weight $ _weight wt <> lbl `scalarMultiply` _point pt

request :: Label -> Weight -> Point -> Point
request lbl wt pt = undefined

--perceptron :: Learner Weight Point Label
--perceptron = Learner implement undefined request

train :: Weight -> Data -> Weight
train = foldr update


---------------
---- Tests ----
---------------

instance (Arbitrary a) => Arbitrary (Vector a) where
  arbitrary = Vector <$> arbitrary

testIdentity :: Vector Float -> Bool
testIdentity vsum = (vsum <> mempty == vsum) && (mempty <> vsum == vsum)

testAssoc :: Vector Integer -> Vector Integer -> Vector Integer -> Bool
testAssoc v1 v2 v3 = (v1 <> (v2 <> v3)) == ((v1 <> v2) <> v3)

runTests = do
  putStrLn "Testing Identity"
  quickCheck testIdentity
  putStrLn "Testing Associativity"
  quickCheck testAssoc
